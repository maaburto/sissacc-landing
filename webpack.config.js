const HtmlWebpackPluginObj = {
  title: 'ReactReduxSassStarterkit',
  inject: false,
  minify: {
    collapseWhitespace: true,
    removeComments: true
  },
  template: require('html-webpack-template'),
  bodyHtmlSnippet: '<main class="main" id="app"></main>',
  meta: [{
    name: 'viewport',
    content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
  }]
};

const constants = {
  'process.env': {
    'API': JSON.stringify(process.env.API),
    'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development' )
  }
};

module.exports = { HtmlWebpackPluginObj, constants };
