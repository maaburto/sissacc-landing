const webpack = require('webpack');
const config = require('./webpack.config');
const path = require('path');

const entries = ['./src/js/index.js', './src/styles/main.scss'];
const constants = config.constants;

const outputs = {
  publicPath: '/',
  filename: 'js/[name].[hash].js',
  chunkFilename: 'js/[name].[hash].js',
  path: path.resolve(__dirname, 'dist')
};

const modules = {
  rules: [{
    test: /\.jsx?$/,
    exclude: [/node_modules/],
    use: ['babel-loader']
  }, {
    enforce: 'pre',
    test: /\.(js|jsx)$/,
    exclude: [/node_modules/],
    loaders: 'eslint-loader'
  }, {
    test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
    loader: 'file-loader'
  }]
};

const plugins = [
  new webpack.DefinePlugin(constants),
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery'
  })
];

module.exports = {
  entry: entries,
  output: outputs,
  module: modules,
  plugins: plugins
};
