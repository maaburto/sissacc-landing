import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import Loadable from 'react-loadable';
import item from '../Item/route';

const routes = [...item];

const App = Loadable({
  loader: () => import(/* webpackChunkName: "App" */ '../app.jsx'),
  loading: () => <div>Loading...</div>
});

const NoMatch = Loadable({
  loader: () => import(/* webpackChunkName: "NoMatch" */ '../Utils/404/noMatch.jsx'),
  loading: () => <div>Loading...</div>
});

// wrap <Route> and use this everywhere instead, then when
// sub routes are added to any route it'll work
function RouteWithSubRoutes(route) {
  return (
    <Route
      exact={route.exact}
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i.toString()} {...route} />
        ))}
        <Route component={NoMatch} />
      </Switch>
    </Router>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object
};

Root.defaultProps = {
  store: {}
};

export default Root;
