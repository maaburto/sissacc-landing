import { applyMiddleware, compose, createStore } from 'redux';

import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxPromise from 'redux-promise';
import thunk from 'redux-thunk';

import SagaManager from './manager/saga';
import staticSagas from './static/sagas';

import ReducerManager from './manager/reducer';
import staticReducers from './static/reducers';

let store;
const reducerManager = new ReducerManager(staticReducers, {});
// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// Create an inject reducer function
// This function adds the async reducer, and creates a new combined reducer
function injectReducer(key, asyncReducer) {
  store.asyncReducers[key] = asyncReducer;
  store.replaceReducer(reducerManager.createReducer(store.asyncReducers));
}

function configureStore(initialState) {
  let storeParams = [];

  if (process.env.NODE_ENV === 'development') {
    storeParams = [
      reducerManager.createReducer(),
      initialState,
      composeWithDevTools(
        applyMiddleware(ReduxPromise),
        applyMiddleware(thunk),
        applyMiddleware(sagaMiddleware)
      )
    ];
  } else {
    storeParams = [
      reducerManager.createReducer(),
      initialState,
      compose(
        applyMiddleware(thunk),
        applyMiddleware(sagaMiddleware)
      )
    ];
  }

  store = createStore(...storeParams);

  // Add a dictionary to keep track of the registered async reducers
  store.asyncReducers = reducerManager.asyncReducers;

  store.injectReducer = injectReducer;

  // run the saga
  const sagaManager = new SagaManager();
  store.sagaManager = sagaManager;
  sagaManager.create(sagaMiddleware.run, staticSagas);

  return store;
}

export default configureStore;
