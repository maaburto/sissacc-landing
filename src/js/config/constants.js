const { API } = process.env;
const APP_NAME = 'Sissa S.A';

export { API, APP_NAME };
