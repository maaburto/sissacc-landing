import { combineReducers } from 'redux';

class ReducerManager {
  constructor(staticReducers, asyncReducers) {
    this.asyncReducers = asyncReducers;
    this.staticReducers = staticReducers;
    this.asyncReducers = asyncReducers;
  }

  createReducer(asyncReducers) {
    const reducers = {
      ...this.staticReducers,
      ...asyncReducers
    };

    return combineReducers(reducers);
  }
}

export default ReducerManager;
