import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Header from './Utils/components/Header';
import Slider from './Utils/components/Slider';
import Blockquote from './Utils/components/Blockquote';
import BigBlock1 from './Utils/components/Bigblock/block1';
import BigBlock2 from './Utils/components/Bigblock/block2';
import BigBlock3 from './Utils/components/Bigblock/block3';
import BigBlock4 from './Utils/components/Bigblock/block4';

@withRouter
@connect()
class App extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor() {
    super();

    this.state = {
      block: {
        experience: {
          style: { backgroundColor: '#c3c6c8', color: '#1b1e57' },
          text: 'Better engagement creates better business outcomes.'
        },
        engagement: {
          style: { backgroundColor: '#006096', color: 'white' },
          text: 'Better experiences create better engagement.'
        },
        partner: {
          style: { backgroundColor: '#0093c9', color: 'white' },
          text:
            'We partner with some of the most ambitious, progressive business executives around the world.'
        },
        believe: {
          style: { backgroundColor: '#7f7f73', color: 'white' },
          text: 'We believe experience is everything. Period'
        }
      }
    };
  }

  componentDidMount() {}

  componentWillUnmount() {
    console.log('Will be Destroyed!!!!');
  }

  openNav = (e, elName) => {
    document.getElementById(elName).style.width = '250px';
    document.getElementById(elName).style.zIndex = 9;
  };

  closeNav = (e, elName) => {
    document.getElementById(elName).style.width = '0';
    document.getElementById(elName).style.zIndex = 0;
  };

  render() {
    const { openNav, closeNav } = this;
    const {
      block: { experience, engagement, partner, believe }
    } = this.state;

    return (
      <>
        <Header openNav={openNav} closeNav={closeNav} />
        <Slider />
        <Blockquote
          text={experience.text}
          blockStyle={experience.style}
          animationMove="fadeInRight"
          dataWowDelay="0s"
        />
        <BigBlock1 />
        <Blockquote
          text={engagement.text}
          blockStyle={engagement.style}
          animationMove="fadeInLeft"
          dataWowDelay="1s"
        />
        <BigBlock2 />
        <Blockquote
          text={partner.text}
          blockStyle={partner.style}
          animationMove="fadeInRight"
          dataWowDelay="1s"
        />
        <BigBlock3 />
        <Blockquote
          text={believe.text}
          blockStyle={believe.style}
          animationMove="fadeInRight"
          dataWowDelay="1s"
        />
        <BigBlock4 />
      </>
    );
  }
}

export default App;
