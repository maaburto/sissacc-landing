const itemForm = {
  id: -1,
  label: null,
  username: null,
  password: null
};

const isLoading = false;
const hasErrored = false;
const request = false;

function itemRequest(state = request, action) {
  switch (action.type) {
    case 'ITEM_REQUEST':
      return true;
    default:
      return state;
  }
}

function itemHasErrored(state = hasErrored, action) {
  switch (action.type) {
    case 'ITEM_HAS_ERRORED':
      return action.hasErrored;
    default:
      return state;
  }
}

function itemIsLoading(state = isLoading, action) {
  switch (action.type) {
    case 'ITEM_IS_LOADING':
      return action.isLoading;
    default:
      return state;
  }
}

function item(state = itemForm, action) {
  switch (action.type) {
    case 'ITEM_FETCH_DATA_SUCCESS':
      return action.item;
    case 'ITEM_FETCH_DATA_EMPTY':
      return state;
    default:
      return state;
  }
}

export { item, itemRequest, itemHasErrored, itemIsLoading };
