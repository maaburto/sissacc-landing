import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

const mapStateToProps = state => {
  return {
    item: state.item,
    hasErrored: state.itemHasErrored,
    isLoading: state.itemIsLoading
  };
};

@withRouter
@connect(
  mapStateToProps,
  null
)
class Item extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    match: PropTypes.object,
    item: PropTypes.object,
    hasErrored: PropTypes.bool,
    isLoading: PropTypes.bool
  };

  static defaultProps = {
    dispatch: () => {},
    match: {},
    item: {},
    hasErrored: false,
    isLoading: false
  };

  componentDidMount() {
    const {
      dispatch,
      match: {
        params: { itemId }
      }
    } = this.props;
    dispatch({ type: 'ITEM_REQUEST', itemId });
  }

  render() {
    const { item, hasErrored, isLoading } = this.props;

    if (hasErrored) {
      return <p>Sorry! There was an error loading the items</p>;
    }

    if (isLoading) {
      return <p>Loading…</p>;
    }

    return (
      <>
        <p>
          <span>ID =</span>
          &nbsp;
          <span>{item.id}</span>
        </p>

        <p>
          <span>Label =</span>
          &nbsp;
          <span>{item.label}</span>
        </p>

        <p>
          <span>Username =</span>
          &nbsp;
          <span>{item.username}</span>
        </p>

        <p>
          <span>Password =</span>
          &nbsp;
          <span>{item.password}</span>
        </p>
      </>
    );
  }
}

export default Item;
