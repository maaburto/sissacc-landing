import React from 'react';
import Loadable from 'react-loadable';
import configureStore from '../config/store';

import { itemListSagas, itemFormSagas } from './lazy/sagas';
import { itemListReducers, itemFormReducers } from './lazy/reducers';

const store = configureStore();
const items = 'items';

const ItemList = Loadable({
  loader: () =>
    import(/* webpackChunkName: "ItemList" */ './list/itemList.jsx').then(module => {
      itemListReducers(store.injectReducer);
      itemListSagas(store.sagaManager);

      return module;
    }),
  loading: () => <div>Loading...</div>
});

const Itemform = Loadable({
  loader: () =>
    import(/* webpackChunkName: "Itemform" */ './form/itemForm.jsx').then(module => {
      itemFormReducers(store.injectReducer);
      itemFormSagas(store.sagaManager);

      return module;
    }),
  loading: () => <div>Loading...</div>
});

const routes = [
  {
    path: `/${items}`,
    component: ItemList,
    exact: true
  },
  {
    path: `/${items}/:itemId`,
    component: Itemform,
    exact: false
  }
];

export default routes;
