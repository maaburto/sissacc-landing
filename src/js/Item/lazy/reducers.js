import * as itemList from '../list/itemList.reducer';
import * as itemForm from '../form/itemForm.reducer';

export function itemListReducers(injectReducer) {
  Object.keys(itemList).forEach(key => {
    injectReducer(key, itemList[key]);
  });
}

export function itemFormReducers(injectReducer) {
  Object.keys(itemForm).forEach(key => {
    injectReducer(key, itemForm[key]);
  });
}
