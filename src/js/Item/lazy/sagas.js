import watchItemsFetchData from '../list/itemList.action';

import watchItemFetchData from '../form/itemForm.action';

export function itemListSagas(sagaManager) {
  const watchItems = {
    watchItemsFetchData
  };

  Object.keys(watchItems).forEach(key => {
    sagaManager.inject(key, watchItems[key]);
  });
}

export function itemFormSagas(sagaManager) {
  const watchItem = {
    watchItemFetchData
  };

  Object.keys(watchItem).forEach(key => {
    sagaManager.inject(key, watchItem[key]);
  });
}
