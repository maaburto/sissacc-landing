const noMatch = false;

function noURLMatch(state = noMatch, action) {
  switch (action.type) {
    case 'NO_MATCH_DATA_URL':
      return action.noMatch;
    default:
      return state;
  }
}

export { noURLMatch as default };
