import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import noURLMatch from './noMatch.action';

@withRouter
@connect(
  null,
  null
)
class NoMatchURL extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    location: PropTypes.object
  };

  static defaultProps = {
    dispatch: () => {},
    location: {}
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(noURLMatch(true));
  }

  render() {
    const { location } = this.props;

    return (
      <>
        <h3>
          No match for
          <code>{location.pathname}</code>
        </h3>
      </>
    );
  }
}

export default NoMatchURL;
