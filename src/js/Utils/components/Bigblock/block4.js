// #region load dependencies
import { jarallax, jarallaxElement } from 'jarallax';
import React, { useEffect, useRef } from 'react';
// import PropTypes from 'prop-types';

// #endregion

// #region components
// #endregion
// #region service
// #endregion
// #region constant
import imageGreat from '../../../../img/backgrounds/blocks/4.jpg';
// #endregion

const BigBlock4 = () => {
  const jarallaxEl = useRef(null);

  useEffect(() => {
    jarallax(jarallaxEl.current, {
      speed: 0.2
    });
    jarallaxElement();

    // Specify how to clean up after this effect:
    return () => {
      jarallax(jarallaxEl.current, 'destroy');
    };
  }, []);

  return (
    <div
      ref={jarallaxEl}
      className="block-4 bg-img bg-overlay"
      style={{
        backgroundImage: `url(${imageGreat})`
      }}
    />
  );
};

/**
 * @name BigBlock4 PropTypes
 * @memberof Utils/components/BigBlock4
 * @type {propTypes} * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
BigBlock4.propTypes = {};

BigBlock4.defaultProps = {};

export default BigBlock4;
