// #region load dependencies
import { jarallax, jarallaxElement } from 'jarallax';
import React, { useEffect, useRef } from 'react';
// import PropTypes from 'prop-types';
import { Row, Container, Col } from 'react-bootstrap';

// #endregion

// #region components
import AnimateText from '../Animation/Text';
// #endregion
// #region service
// #endregion
// #region constant
import imagePartner from '../../../../img/backgrounds/blocks/3.jpg';
// #endregion

const BigBlock3 = () => {
  const jarallaxEl = useRef(null);

  useEffect(() => {
    jarallax(jarallaxEl.current, {
      speed: 0.2
    });
    jarallaxElement();

    // Specify how to clean up after this effect:
    return () => {
      jarallax(jarallaxEl.current, 'destroy');
    };
  }, []);

  return (
    <div
      ref={jarallaxEl}
      className="block-3 bg-img bg-overlay"
      style={{
        backgroundImage: `url(${imagePartner})`
      }}
    >
      <Container className="container--block-3">
        <Row>
          <Col xs={12}>
            <AnimateText
              tag="h2"
              text="Our Client Customers"
              targetClass="text-block-3"
              animationMove="fadeInRight"
              dataWowDelay="1s"
            />
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <AnimateText
              tag="h2"
              text="live better"
              targetClass="text-1-block-3"
              animationMove="fadeInRight"
              dataWowDelay="1.5s"
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

/**
 * @name BigBlock3 PropTypes
 * @memberof Utils/components/BigBlock3
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
BigBlock3.propTypes = {};

BigBlock3.defaultProps = {};

export default BigBlock3;
