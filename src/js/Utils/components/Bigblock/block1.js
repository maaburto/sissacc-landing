// #region load dependencies
import { jarallax, jarallaxElement } from 'jarallax';
import React, { useEffect, useRef } from 'react';
// import PropTypes from 'prop-types';
// import { Row, Container, Col } from 'react-bootstrap';

// #endregion

// #region components
import AnimateImage from '../Animation/Image';
// #endregion
// #region service
// #endregion
// #region constant
import one from '../../../../img/backgrounds/blocks/1.jpg';
import imgBussines from '../../../../img/backgrounds/blocks/bussines.png';
// #endregion

const BigBlock1 = () => {
  const jarallaxEl = useRef(null);

  useEffect(() => {
    jarallax(jarallaxEl.current, {
      speed: 0.2
    });
    jarallaxElement();

    // Specify how to clean up after this effect:
    return () => {
      jarallax(jarallaxEl.current, 'destroy');
    };
  }, []);

  return (
    <div
      ref={jarallaxEl}
      className="block-1"
      style={{
        backgroundImage: `url(${one})`
      }}
    >
      <AnimateImage
        alt="bussines"
        targetClass="img-block-1"
        srcImage={imgBussines}
        animationMove="rollIn"
        dataWowDelay="1s"
      />
      {/* <img className="img-block-1" alt="bussines" src={imgBussines} /> */}
    </div>
  );
};

/**
 * @name BigBlock1 PropTypes
 * @memberof Utils/components/BigBlock1
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
BigBlock1.propTypes = {};

BigBlock1.defaultProps = {};

export default BigBlock1;
