// #region load dependencies
import { jarallax, jarallaxElement } from 'jarallax';
import React, { useEffect, useState, useRef } from 'react';
// import PropTypes from 'prop-types';

// #endregion

// #region components
// #endregion
// #region service
// #endregion
// #region constant
import imageUnion from '../../../../img/backgrounds/blocks/2.jpg';
// #endregion

const BigBlock2 = () => {
  const jarallaxEl = useRef(null);
  const [counter, setCounter] = useState(1);
  const [isRunning, setIsRunning] = useState(true);
  const target = 100;

  function useInterval(callback, delay) {
    const savedCallback = useRef();

    // Remember the latest callback.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);

    // Set up the interval.
    // eslint-disable-next-line consistent-return
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }

      if (delay !== null) {
        const id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

  useInterval(
    () => {
      if (counter === target - 1) {
        setIsRunning(false);
      }

      setCounter(counter + 1);
    },
    isRunning ? 10 : null
  );

  useEffect(() => {
    jarallax(jarallaxEl.current, {
      speed: 0.2
    });
    jarallaxElement();

    // Specify how to clean up after this effect:
    return () => {
      jarallax(jarallaxEl.current, 'destroy');
    };
  }, []);

  return (
    <div
      ref={jarallaxEl}
      className="block-2 bg-img bg-overlay"
      style={{
        backgroundImage: `url(${imageUnion})`
      }}
    >
      <div className="circle">{counter}</div>
    </div>
  );
};

/**
 * @name BigBlock2 PropTypes
 * @memberof Utils/components/BigBlock2
 * @type {propTypes} * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
BigBlock2.propTypes = {};

BigBlock2.defaultProps = {};

export default BigBlock2;
