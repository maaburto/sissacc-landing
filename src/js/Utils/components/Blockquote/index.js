// #region load dependencies
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { WOW } from 'wowjs/dist/wow';
// #endregion

// #region components
// #endregion
// #region service
// #endregion
// #region constant
// #endregion

const Blockquote = ({ text, blockStyle, animationMove, dataWowDelay }) => {
  const customBlockStyle = {
    ...blockStyle,
    fontWeight: '600'
  };

  const customClass = `wow ${animationMove} text`;

  useEffect(() => {
    new WOW({
      live: false
    }).init();
  });

  return (
    <div className="blockquote" style={customBlockStyle}>
      <span className={customClass} data-wow-duration="1s" data-wow-delay={dataWowDelay}>
        {text}
      </span>
    </div>
  );
};

/**
 * @name Blockquote PropTypes
 * @memberof Utils/components/Blockquote
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
Blockquote.propTypes = {
  text: PropTypes.string.isRequired,
  blockStyle: PropTypes.object.isRequired,
  animationMove: PropTypes.string,
  dataWowDelay: PropTypes.string
};

Blockquote.defaultProps = {
  animationMove: '',
  dataWowDelay: '0s'
};

export default Blockquote;
