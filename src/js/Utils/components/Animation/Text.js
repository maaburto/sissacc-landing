// #region load dependencies
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { WOW } from 'wowjs/dist/wow';
// #endregion

// #region components
// #endregion
// #region service
// #endregion
// #region constant
// #endregion

const AnimateImage = ({ tag, targetClass, animationMove, dataWowDelay, text, dataWowOffSet }) => {
  const CustomTag = `${tag}`;
  const customClass = `${targetClass} wow ${animationMove}`;

  useEffect(() => {
    new WOW({
      live: false
    }).init();
  });

  return (
    <CustomTag
      className={customClass}
      data-wow-delay={dataWowDelay}
      data-wow-offset={dataWowOffSet}
      data-wow-duration="1s"
    >
      {text}
    </CustomTag>
  );
};

/**
 * @name AnimateImage PropTypes
 * @memberof Utils/components/AnimateImage
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
AnimateImage.propTypes = {
  text: PropTypes.string.isRequired,
  targetClass: PropTypes.string,
  tag: PropTypes.string.isRequired,
  animationMove: PropTypes.string.isRequired,
  dataWowDelay: PropTypes.string.isRequired,
  dataWowOffSet: PropTypes.number
};

AnimateImage.defaultProps = {
  dataWowOffSet: 1,
  targetClass: ''
};

export default AnimateImage;
