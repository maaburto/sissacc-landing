// #region load dependencies
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { WOW } from 'wowjs/dist/wow';
// #endregion

// #region components
// #endregion
// #region service
// #endregion
// #region constant
// #endregion

const Animate = ({ tag, dataAnimation, dataDelay, text, dataWowOffSet }) => {
  const CustomTag = `${tag}`;
  const customMoveClass = `wow ${dataAnimation}`;

  useEffect(() => {
    new WOW({
      live: false
    }).init();
  });

  return (
    <CustomTag
      className={customMoveClass}
      data-animation={dataAnimation}
      data-delay={dataDelay}
      data-wow-offset={dataWowOffSet}
    >
      {text}
    </CustomTag>
  );
};

/**
 * @name Animate PropTypes
 * @memberof Utils/components/Animate
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
Animate.propTypes = {
  text: PropTypes.string.isRequired,
  tag: PropTypes.string.isRequired,
  dataAnimation: PropTypes.string.isRequired,
  dataDelay: PropTypes.string.isRequired,
  dataWowOffSet: PropTypes.number
};

Animate.defaultProps = {
  dataWowOffSet: 1
};

export default Animate;
