// #region load dependencies
import $ from 'jquery';
import 'owl.carousel';
import { jarallax, jarallaxElement } from 'jarallax';
import React, { useEffect, useRef } from 'react';
// import PropTypes from 'prop-types';
import { Row, Container, Col } from 'react-bootstrap';

// import { faBars } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// #endregion

// #region components
import Animation from '../Animation';
// #endregion
// #region service
// #endregion
// #region constant
import one from '../../../../img/backgrounds/1.jpg';
import two from '../../../../img/backgrounds/1.1.jpg';
// #endregion

const Slider = () => {
  const mainCarouselClass = '.owl-carousel';
  const jarallaxEl = useRef(null);

  const owlCallbackMove = () => {
    const slideLayer = $('[data-animation]');
    slideLayer.each(index => {
      const target = slideLayer[index];
      const animName = $(target).data('animation');
      $(target)
        .removeClass(`animated ${animName}`)
        .css('opacity', '0');
    });
  };

  const owlCallbackMoved = () => {
    const mainSlider = $(mainCarouselClass);
    const slideLayer = mainSlider.find('.owl-item.active').find('[data-animation]');
    slideLayer.each(index => {
      const target = slideLayer[index];
      const animName = $(target).data('animation');
      $(target)
        .addClass(`animated ${animName}`)
        .css('opacity', '1');
    });
  };

  useEffect(() => {
    if ($.fn.owlCarousel) {
      const mainSlider = $(mainCarouselClass);
      const owlOpts = {
        items: 1,
        margin: 0,
        loop: true,
        dots: false,
        autoplay: false,
        autoplayTimeout: 5000,
        smartSpeed: 1000
      };

      mainSlider.owlCarousel(owlOpts);

      mainSlider.on('translate.owl.carousel', owlCallbackMove);
      mainSlider.on('translated.owl.carousel', owlCallbackMoved);

      const dataDelay = $('[data-delay]');
      const dataDuration = $('[data-duration]');

      dataDelay.each(index => {
        const slideLayer = dataDelay[index];
        const animDelay = $(slideLayer).data('delay');
        $(slideLayer).css('animation-delay', animDelay);
      });

      dataDuration.each(index => {
        const slideLayer = dataDuration[index];
        const animDuration = $(slideLayer).data('duration');
        $(slideLayer).css('animation-duration', animDuration);
      });
    }

    jarallax(jarallaxEl.current, {
      speed: 0.2
    });
    jarallaxElement();

    // Specify how to clean up after this effect:
    return () => {
      jarallax(jarallaxEl.current, 'destroy');
    };
  }, []);

  return (
    <div className="hero-area">
      <div className="owl-carousel">
        {/* <!-- Single Welcome Slides --> */}
        <div
          ref={jarallaxEl}
          className="single-welcome-slides bg-img bg-overlay"
          style={{
            backgroundImage: `url(${one})`
          }}
        >
          <Container className="h-100">
            <Row className="h-100 align-items-center">
              <Col xs lg={10}>
                <div className="welcome-content">
                  <Animation
                    text="Effortless. Intelligent. Personalized."
                    tag="p"
                    dataAnimation="fadeInRight"
                    dataDelay="200ms"
                  />

                  <Animation
                    text="EXPERIENCES"
                    tag="h2"
                    dataAnimation="fadeInRight"
                    dataDelay="1s"
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        {/* <!-- Single Welcome Slides --> */}
        <div
          className="single-welcome-slides bg-img bg-overlay jarallax"
          style={{ backgroundImage: `url(${two})` }}
        >
          <div className="container h-100">
            <div className="row h-100 align-items-center">
              <div className="col-12 col-lg-10">
                <div className="welcome-content">
                  <Animation
                    text="International Standardized Training Rooms."
                    tag="h2"
                    dataAnimation="fadeInRight"
                    dataDelay="0.5s"
                  />

                  <Animation
                    text="We also 1st class worldwide training rooms where we invest resources so that our new employees."
                    tag="p"
                    dataAnimation="fadeInRight"
                    dataDelay="1s"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

/**
 * @name Slider PropTypes
 * @memberof Utils/components/Slider
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
Slider.propTypes = {};

Slider.defaultProps = {};

export default Slider;
