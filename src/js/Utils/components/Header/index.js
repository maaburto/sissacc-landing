// #region load dependencies
import React from 'react';
import PropTypes from 'prop-types';
// #endregion

// #region components
import Menu from '../Menu';
// #endregion

// #region constant
// #endregion

const Header = ({ openNav, closeNav }) => (
  <header className="header-area">
    <Menu openNav={openNav} closeNav={closeNav} />
  </header>
);

/**
 * @name Header PropTypes
 * @memberof Utils/components/Header
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
Header.propTypes = {
  openNav: PropTypes.func,
  closeNav: PropTypes.func
};

Header.defaultProps = {
  openNav: () => {},
  closeNav: () => {}
};

export default Header;
