// /region load dependencies
import React from 'react';
import PropTypes from 'prop-types';

import { Row, Container, Col } from 'react-bootstrap';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

const Menu = ({ openNav, closeNav }) => {
  const sideNavId = 'mySidenav';

  return (
    <Container fluid>
      <Row>
        <Col xs={6} sm={6} md={4}>
          <img
            className="logo-sissacc img-fluid"
            height="60"
            width="240"
            alt="logo"
            src="https://placeholder.com/wp-content/uploads/2018/10/placeholder.com-logo3.png"
          />
        </Col>

        <Col xs={6} sm={6} md={7} className="sissacc-menu">
          <div className="sissacc-menu" id="sissacc-menu">
            <a href="#home" className="active">
              Home
            </a>
            <Link to={{ pathname: `/items` }}>News</Link>
            <a href="#contact">Contact</a>
            <a href="#about">About</a>
            <a onClick={e => openNav(e, sideNavId)} href="#open" className="icon">
              <FontAwesomeIcon icon={faBars} size="2x" />
            </a>
          </div>
        </Col>

        <Col sm={12}>
          <div id={sideNavId} className="sidenav">
            <a href="#button" className="closebtn" onClick={e => closeNav(e, sideNavId)}>
              &times;
            </a>
            <a href="#home">Home</a>
            <a href="news">News</a>
            <a href="#contact">Contact</a>
            <a href="#about">About</a>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

/**
 * @name AlertBox PropTypes
 * @memberof Utils/components/Menu
 * @type {propTypes}
 * @param {Object} props - React PropTypes
 * @return {Array} React PropTypes
 */
Menu.propTypes = {
  openNav: PropTypes.func.isRequired,
  closeNav: PropTypes.func.isRequired
};

Menu.defaultProps = {};

export default Menu;
