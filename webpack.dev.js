const config = require('./webpack.config');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let HtmlWebpackPluginObj = config['HtmlWebpackPluginObj'];
HtmlWebpackPluginObj.minify.collapseWhitespace = false;
HtmlWebpackPluginObj.minify.removeComments = false;

const rules = [{
  test: /\.scss$/,
  use: [{
    loader: "style-loader"
  }, {
    loader: "css-loader"
  }, {
    loader: "sass-loader"
  }]
}];

const plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new HtmlWebpackPlugin(HtmlWebpackPluginObj)
];

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    hot: true,
    historyApiFallback: true
  },
  module: { rules },
  plugins: plugins
});
